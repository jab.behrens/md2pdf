#!/usr/bin/bash

usage()
{
    echo "Syntax: md2pdf filename options"
    echo "options:"
    echo "-c     use --pandoc-citeproc"
    echo "-h     print this help"
    echo "-o     specify name of outputfile (default: filename.pdf)"
    echo
}
help()
{
    # Display Help
    echo "bash-script to convert markdown to pdf using pandoc"
    echo
    usage
}
#pandocPath="/home/jab/.pandoc"
pandocPath=$(dirname -- "$( readlink -f -- "$0"; )")
cite=false
luafilter=true
mermaid=false
fname=$1
mdTypeCheck='(\.md)$'
pdfTypeCheck='(\.pdf)$'
filter="--filter pandoc-xnos"
if [[ "$fname" == "-"* ]] ; then
    echo 'Error: Missing filename'
    echo
    echo 'correct usage:'
    echo
    usage
    exit
elif [[ "$fname" =~ $mdTypeCheck ]] ; then
    fname=${fname: 0:-3}
fi
outputfname=$fname
# drop first argument
shift
while getopts ":hcfmo::" option; do
    case $option in
        h) # display Help
            help
            exit;;
        c) cite=true;;
        f) luafilter=false;;
        m) mermaid=true;;
        o) # Enter a name
            outputfname=$OPTARG
            if [[ "$outputfname" =~ $pdfTypeCheck ]] ; then
                outputfname=${outputfname: 0:-4}
            fi;;
        \?) # Invalid option
            echo "Error: Unknown option -$OPTARG"
            exit;;
        \:) # no input to option found
            echo "Error: Missing parameter for -$OPTARG";
            exit;;
    esac
done

echo "$fname.md -> $outputfname.pdf"
echo "$(<$fname.md)" | sed -z 's/\n\n\n/\n\n\\clearpage\n\n/g' > /tmp/md2pdf.md

if [[ "$luafilter" = true ]] ; then
    filter="$filter --lua-filter=$pandocPath/filterPandoc.lua"
fi
if [[ "$cite" = true ]] ; then
    filter="$filter -V breakurl -V hyphens=URL -F pandoc-citeproc"
fi
if [[ "$mermaid" = true ]] ; then
    filter="$filter -F mermaid-filter"
fi
filter="$filter --lua-filter=$pandocPath/glossaries.lua"
filter="$filter --lua-filter=$pandocPath/pandoc-quotes.lua"

template="--template=$pandocPath/templatePandoc.latex"
#template="--template=$pandocPath/default.latex"
pdfEngine="--pdf-engine=xelatex"
#pdfEngine="--pdf-engine=lualatex"
metadata=" --metadata-file=$pandocPath/metaPandoc.yml"

pandoc $template $filter $pdfEngine $metadata -s "/tmp/md2pdf.md" -o "$outputfname.pdf"
