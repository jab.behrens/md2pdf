# md2pdf

Convert markdown to pdf using pandoc and additional filter.

# Usage

```bash
md2pdf filename options
```
options:
```
-c    use pandoc-citeproc
-f    don't use custom lua-filter
-h    print help
-m    use mermaid-filter
-o    specify name of outputfile (default: filename.pdf)
```

## Examples

```bash
md2pdf input.md # convert input.md to input.pdf
md2pdf input # short form
md2pdf input.md -o output.pdf # convert input.md to output.pdf
md2pdf input -o output # short form
md2pdf input.md -c # convert input.md to input.pdf using pandoc-citeproc
```

# Dependencies

- [`pandoc`](https://www.pandoc.org): `sudo apt install pandoc`
- [`pandoc-xnos`](https://github.com/tomduck/pandoc-xnos): `pip install pandoc-fignos pandoc-eqnos pandoc-tablenos pandoc-secnos --user`
- `xelatex`: `sudo apt install texlive-xetex`

*optional*:
- [`mermaid-filter`](https://github.com/raghur/mermaid-filter): `npm install --global mermaid-filter`

# Installation

- download this repository

- change name in `metaPandoc.yml`

# Features

- two empty lines become a pagebreak
- set section number:  
  ```
  # 3 Section
  ##   5 Subsection
  ### 2 5 7 Subsubsection
  ```
  becomes:
  ```
  \setcounter{section}{3}
  # Section
  \setcounter{subsection}{5}
  ## Subsection
  \setcounter{section}{2}
  \setcounter{subsection}{5}
  \setcounter{subsubsection}{7}
  ### Subsubsection
  ```
- siunitx: (`/m` for `\metre`; `/T` for `\tesla`; `/d` for `\day`; `/h` for `\hour`; `/M` for `\nauticalmile`)
    - `<A.s./.V././m>` -> `\si{\ampere\second\per\volt\per\metre}`
    - `<10 m.A.s>` -> `\SI{10}{\milli\ampere\second}`
    - `<10-25 A.s>` -> `\SIrange{10}{25}{\ampere\second}`
    - `<10;12;15;19 A.s>` -> `\SIlist{10;12;15;19}{\ampere\second}`
- math mode:
    - `……{TEXT}……` -> `\underline{\underline{TEXT}}` (underline TEXT twice)
    - `…{TEXT}…` -> `\underline{TEXT}` (underline TEXT)
    - `•` -> ` \cdot `
    - `…` -> ` \cdots `
    - `⇒` -> ` \\Rightarrow `
    - `⇔` -> ` \Leftrightarrow `
    - `⇐` -> ` \Leftarrow `
    - `=>` -> ` \Rightarrow `
    - `<=>` -> ` \Leftrightarrow `
    - `->` -> ` \rightarrow `
    - `<->` -> ` \leftrightarrow `
    - `<-` -> ` \leftarrow `
    - `α` -> ` \alpha `
    - `π` -> ` \pi `
    - `sin` -> ` \sin`
    - `cos` -> ` \cos`
    - `arcsin` -> ` \arcsin`
    - `arccos` -> ` \arccos`
    - `tan` -> ` \tan`
    - `cot` -> ` \cot`
    - `log` -> ` \log`
    - `ln` -> ` \ln`
    - `lg` -> ` \lg`
    - `exp` -> ` \exp`
    - `(x || y || z)` -> `\begin{pmatrix} x \\ y \\ z \end{pmatrix}` (vector)
    - `\[` -> `\left[`
    - `\{` -> `\left{`
    - `\]` -> `\right]`
    - `\}` -> `\right}`
    - `(` -> `\left(`
    - `)` -> `\right)`
    - `!=` -> ` \neq`
    - `<=` -> ` \leq`
    - `>=` -> ` \geq`
    - `~=` -> ` \approx`
    - `oo` -> ` \infty`
    - `+-` -> ` \pm`
    - `\N` -> `\mathbb{N}` (same for `\Z`, `\Q`, `\R`, `\C`, `\L`)
    - `-F>` -> `\vec{F}` (everything non-whitespace between `-` and `>`)
    - `∪` -> `\cup`
    - `∩` -> `\cap`
    - `\!{TEXT}` -> `\mathsf{TEXT}` (normal font)
