local logging = require 'logging'

-- function Pandoc(pandoc)
--     logging.temp('pandoc', pandoc)
-- end

-- function Blocks(b)
--     logging.temp('pandoc', b)
-- end

function Header(h)
    local sec = {-1,'',-1,'',-1}
    local content = {}
    for index, value in ipairs(h.content) do
        if value.t ~= 'Space' then
            if value.text:find('[%.%d]+') ~= nil then
                sec[index] = tonumber(value.text)
                -- logging.temp('i',sec)
            elseif value.text:find(' ') == nil then
                table.insert(content, value)
            end
        elseif #content ~= 0 then
            table.insert(content, value)
        end
    end
    local res = {}
    if sec[1] ~= -1 then
        res[#res+1] = pandoc.RawBlock('tex', [[\setcounter{section}{]]..sec[1]..[[}]])
    end
    if sec[3] ~= -1 then
        res[#res+1] = pandoc.RawBlock('tex', [[\setcounter{subsection}{]]..sec[3]..[[}]])
    end
    if sec[5] ~= -1 then
        res[#res+1] = pandoc.RawBlock('tex', [[\setcounter{subsubsection}{]]..sec[5]..[[}]])
    end
    res[#res+1] = pandoc.Header(h.level, content, h.attr)
    return res
end

function Meta(m)
    if m.date == nil then
        m.date = os.date("%d. %B %Y")
        return m
    end
end

function Math(m)
    local type = m.mathtype
    local txt = m.text
    txt = txt:gsub('[\n]', [[ \\]]) -- Zeilenumbruch mit \\ ergänzen
    if txt:find(' \\') ~= nil or txt:find('&') ~= nil then
        if txt:find('aligned') == nil then
            txt = [[\begin{aligned}]] .. txt .. [[\end{aligned}]]
        end
    end
    -- ……{text}…… to underline 'text' twice
    txt = txt:gsub([[……{(.+)}……]], [[\underline{\underline{%1}}]])
    -- …{text}… to underline 'text'
    txt = txt:gsub([[…{(.+)}…]], [[\underline{%1}]])
    txt = txt:gsub([[•]], [[ \cdot ]]) -- Multiplikation
    txt = txt:gsub([[…]], [[ \cdots ]]) -- Reihen
    txt = txt:gsub([[⇒]], [[ \\Rightarrow ]])
    txt = txt:gsub([[⇔]], [[ \Leftrightarrow ]])
    txt = txt:gsub([[⇐]], [[ \Leftarrow ]])
    txt = txt:gsub([[=>]], [[ \Rightarrow ]])
    txt = txt:gsub([[<=>]], [[ \Leftrightarrow ]])
    -- txt = txt:gsub([[<=]], [[ \Leftarrow ]])
    txt = txt:gsub([[%->]], [[ \rightarrow ]])
    txt = txt:gsub([[<%->]], [[ \leftrightarrow ]])
    txt = txt:gsub([[<%-]], [[ \leftarrow ]])
    txt = txt:gsub([[α]], [[ \alpha ]])
    txt = txt:gsub([[π]], [[ \pi ]])
    txt = txt:gsub([[sin]], [[ \sin]])
    txt = txt:gsub([[cos]], [[ \cos]])
    txt = txt:gsub([[arcsin]], [[ \arcsin]])
    txt = txt:gsub([[arccos]], [[ \arccos]])
    txt = txt:gsub([[tan]], [[ \tan]])
    txt = txt:gsub([[cot]], [[ \cot]])
    txt = txt:gsub([[log]], [[ \log]])
    txt = txt:gsub([[ln]], [[ \ln]])
    txt = txt:gsub([[lg]], [[ \lg]])
    txt = txt:gsub([[exp]], [[ \exp]])
    -- (x || y || z) as \begin{pmatrix} x \\ y \\ z \end{pmatrix}
    txt = txt:gsub([[%((.+||)]], [[\begin{pmatrix}%1]])
    txt = txt:gsub([[(\begin{pmatrix}[^\n]+)%)]], [[%1\end{pmatrix}]])
    txt = txt:gsub([[||]], [[\\]])

    txt = txt:gsub([[\([%[%{])]], [[\left%1]]) -- \[ to \left[ or \{ to \left{
    txt = txt:gsub([[\([%]%}])]], [[\right%1]]) -- \} to \right\} or \] to \right]
    txt = txt:gsub([[([%(])]], [[\left%1]])
    txt = txt:gsub([[([%)])]], [[\right%1]])
    txt = txt:gsub([[!=]], [[ \neq ]])
    txt = txt:gsub([[<=]], [[ \leq ]])
    txt = txt:gsub([[>=]], [[ \geq ]])
    txt = txt:gsub([[~=]], [[ \approx ]])
    txt = txt:gsub([[oo]], [[ \infty ]])
    txt = txt:gsub([[%+%-]], [[ \pm ]])
    txt = txt:gsub([[\([NZQRCL])%s]], [[\mathbb{%1} ]])
    txt = txt:gsub([[%-([^%s]+)>]], [[\vec{%1}]]) -- define everything between '-' and '>' as vector
    txt = txt:gsub([[∪]], [[\cup ]])
    txt = txt:gsub([[∩]], [[\cap ]])
    txt = txt:gsub([[\!{]], [[\mathsf{]]) -- \!{text} to write 'text' in normal font
    -- txt = txt:gsub([[√%[]], [[ \sqrt[]])
    -- txt = txt:gsub([[√]], [[ \sqrt{]])
    -- -- txt = txt:gsub([[(\sqrt%[.+%])[^{%s]+]], [[%1{]])
    -- txt = txt:gsub([[(\sqrt{.+)%s]], [[%1} ]])
    --  ersetze a^123 durch a^{123}
    -- txt = txt:gsub([[(%^)([%d%a]+)%s]], [[^{%2]])
    -- txt = txt:gsub([[(%^[^%^][^%s]+)(%s)]], [[%1} ]])
    -- ersetze a_123 durch a_{123}
    -- txt = txt:gsub([[(_)([%d%a]+)%s]], [[_{%2]])
    -- txt = txt:gsub([[(_[^{][^%s]+)(%s)]], [[%1} ]])
    -- txt = txt:gsub([[]], [[]])
    txt = txt:gsub([[%s+]], [[ ]])
    -- logging.temp('txt', txt)
    local test = pandoc.Math(type, txt)
    return test
end

function string:split(sep)
   local sep, fields = sep or ":", {}
   local pattern = string.format("([^%s]+)", sep)
   self:gsub(pattern, function(c) fields[#fields+1] = c end)
   return fields
end

function string:expand(key, expanded)
    local includedB = false
    local str = self
    if self == key then
        includedB = true
        str = expanded
    end
    -- if self:find(key) then
    --     includedB = true
    --     str = self:gsub(key, expanded)
    -- end
    return includedB, str
end

local function unitToKey(unit)
    local key = ''
    unit = unit:gsub([[/]], [[&]])
    local splitTable = unit:split('.')
    -- logging.temp('split', splitTable)
    local replaceTable = {
        ['&m'] = [[\metre]],
        ['g'] = [[\gram]],
        ['s'] = [[\second]],
        ['A'] = [[\ampere]],
        ['K'] = [[\kelvin]],
        ['mol'] = [[\mole]],
        ['cd'] = [[\candela]],
        ['l'] = [[\litre]],
        ['L'] = [[\liter]],
        ['Hz'] = [[\hertz]],
        ['N'] = [[\newton]],
        ['Pa'] = [[\pascal]],
        ['O'] = [[\ohm]],
        ['V'] = [[\volt]],
        ['W'] = [[\watt]],
        ['J'] = [[\joule]],
        ['eV'] = [[\electronvol]],
        ['F'] = [[\farad]],
        ['dB'] = [[\decibel]],
        ['2'] = [[\squared]],
        ['3'] = [[\cubic]],
        ['&'] = [[\per]],
        ['rad'] = [[\radian]],
        ['sr'] = [[\steradian]],
        ['C'] = [[\coulomb]],
        ['S'] = [[\siemens]],
        ['Wb'] = [[\weber]],
        ['&T'] = [[\tesla]],
        ['H'] = [[\henry]],
        ['°C'] = [[\degreeCelsius]],
        ['lm'] = [[\lumen]],
        ['lx'] = [[\lux]],
        ['Bq'] = [[\becquerel]],
        ['Gy'] = [[\gray]],
        ['Sv'] = [[\sievert]],
        ['kat'] = [[\katal]],
        ['ha'] = [[\hectare]],
        ['t'] = [[\tonne]],
        ['&d'] = [[\day]],
        ['&h'] = [[\hour]],
        ['min'] = [[\minute]],
        ['°'] = [[\degree]],
        ["'"] = [[\arcminute]],
        ['"'] = [[\arcsecond]],
        ['u'] = [[\atomicmassunit]],
        ['bar'] = [[\bar]],
        ['mmHg'] = [[\mmHg]],
        ['B'] = [[\bel]],
        ['Np'] = [[\neper]],
        ['Ä'] = [[\angstrom]],
        ['&M'] = [[\nauticalmile]],
        ['ua'] = [[\astronomicalunit]],
        ['Y'] = [[\yotta]],
        ['Z'] = [[\zetta]],
        ['E'] = [[\exa]],
        ['P'] = [[\peta]],
        ['T'] = [[\tera]],
        ['G'] = [[\giga]],
        ['M'] = [[\mega]],
        ['k'] = [[\kilo]],
        ['h'] = [[\hecto]],
        ['da'] = [[\deca]],
        ['d'] = [[\deci]],
        ['c'] = [[\centi]],
        ['m'] = [[\milli]],
        ['μ'] = [[\micro]],
        ['n'] = [[\nano]],
        ['p'] = [[\pico]],
        ['f'] = [[\femto]],
        ['a'] = [[\atto]],
        ['z'] = [[\zepto]],
        ['y'] = [[\yocto]],
    }
    for i, val in pairs(splitTable) do
        local keyAdd, found
        for findKey, expanded in pairs(replaceTable) do
            found, keyAdd = val:expand(findKey, expanded)
            if found then break end
        end
        key = key .. keyAdd
        -- logging.temp('key', key)
    end
    return key
end

local siNum = 0
function Str(str)
    local txt = str.text
    if txt:sub(0,1)..txt:sub(-1) == '<>' then
        local si = [[\si{]]..unitToKey(txt:sub(2,-2))..'}'
        -- logging.temp('SI', si)
        return pandoc.RawInline('tex', si)
    elseif txt:sub(0,1) == '<' then
        siNum = txt:sub(2)
        -- logging.temp('si', txt:sub(2))
        return pandoc.Str('')
    elseif txt:sub(-1) == '>' then
        local si
        if siNum:find('-') then
            si = [[\SIrange{]]..siNum:gsub('-', '}{')..'}{'..unitToKey(txt:sub(0,-2))..'}'
        elseif siNum:find(';') then
            si = [[\SIlist{]]..siNum..'}{'..unitToKey(txt:sub(0,-2))..'}'
        else
            si = [[\SI{]]..siNum..'}{'..unitToKey(txt:sub(0,-2))..'}'
        end
        -- logging.temp('si', si)
        return pandoc.RawInline('tex', si)
    end
    return str
end

-- function Para(p)
--     logging.temp('p',p)
-- end
